# Go Notebooks for VSCode

> Powered by [Yaegi](https://github.com/traefik/yaegi)

A notebook extension for VSCode that uses Yaegi for evaluating Go.