const path = require('path')

module.exports = {
    entry: './src/renderer/main.ts',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist', 'renderer'),
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
}
