module gitlab.com/firelizzard/vscode-go-notebooks/kernel

go 1.15

require (
	github.com/davecgh/go-spew v1.1.0
	github.com/denisenkom/go-mssqldb v0.9.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.5.1 // indirect
	github.com/lib/pq v1.10.0
	github.com/traefik/yaegi v0.9.15
	google.golang.org/grpc v1.36.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0 // indirect
	google.golang.org/protobuf v1.26.0
)
