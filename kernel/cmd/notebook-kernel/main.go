package main

import (
	"bytes"
	"context"
	"errors"
	"flag"
	"fmt"
	"go/build"
	"io"
	"log"
	"net"
	"os"
	"sync"
	"time"

	"github.com/traefik/yaegi/interp"
	"github.com/traefik/yaegi/stdlib"
	"gitlab.com/firelizzard/vscode-go-notebooks/kernel/proto"
	grpc "google.golang.org/grpc"
)

var testMode = flag.Bool("test", false, "Test mode")

const usage = `
Usage: %[1]s <cmd> [<args>]

	%[1]s stdio
	%[1]s tcp <addr>
`

func printUsage() {
	fmt.Fprintf(os.Stderr, usage[1:], os.Args[0])
	os.Exit(1)
}

func main() {
	if len(os.Args) < 2 {
		printUsage()
	}

	var l net.Listener
	var err error

	switch os.Args[1] {
	case "stdio":
		if len(os.Args) != 2 {
			printUsage()
		}

		l = new(listener)

	case "tcp":
		if len(os.Args) != 3 {
			printUsage()
		}

		l, err = net.Listen("tcp", os.Args[2])
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}

		fmt.Printf("Listening on %v\n", l.Addr())
	}

	k := new(kernel)
	s := grpc.NewServer()
	proto.RegisterKernelServer(s, k)

	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func newInterpreter(ctx context.Context) (*interp.Interpreter, <-chan []byte, <-chan []byte, context.Context, context.CancelFunc) {
	ctx, cancel := context.WithCancel(ctx)

	// rIn, wIn := io.Pipe()
	rOut, wOut := io.Pipe()
	rErr, wErr := io.Pipe()

	go func() {
		<-ctx.Done()
		rOut.Close()
		wOut.Close()
		rErr.Close()
		wErr.Close()
	}()

	gopath := os.Getenv("GOPATH")
	if gopath == "" {
		gopath = build.Default.GOPATH
	}

	I := interp.New(interp.Options{
		Stdin:  bytes.NewReader(nil),
		Stdout: wOut,
		Stderr: wErr,
		GoPath: gopath,
	})
	I.Use(stdlib.Symbols)
	// I.Use(syscall.Symbols)

	chOut := make(chan []byte)
	chErr := make(chan []byte)

	go forwardIO(ctx, rOut, chOut)
	go forwardIO(ctx, rErr, chErr)

	return I, chOut, chErr, ctx, cancel
}

func forwardIO(ctx context.Context, r io.Reader, ch chan<- []byte) {
	defer close(ch)

	var b [512]byte
	for {
		n, err := r.Read(b[:])
		if err != nil {
			if !errors.Is(err, io.ErrClosedPipe) {
				log.Fatal(err)
			}
			return
		}

		d := make([]byte, n)
		copy(d, b[:n])
		ch <- d
	}
}

func sendIO(ctx context.Context, wg *sync.WaitGroup, ch <-chan []byte, send func([]byte)) {
	defer wg.Done()

	for {
		select {
		case <-ctx.Done():
			return

		case b := <-ch:
			send(b)
		}
	}
}

type listener struct {
	mu sync.Mutex
}

func (l *listener) Accept() (net.Conn, error) {
	l.mu.Lock()
	return connection{&l.mu}, nil
}

func (l *listener) Close() error {
	os.Stdin.Close()
	os.Stdout.Close()
	return nil
}

func (l *listener) Addr() net.Addr {
	return address{}
}

type connection struct {
	mu *sync.Mutex
}

func (c connection) Close() error {
	c.mu.Unlock()
	return nil
}

func (c connection) SetDeadline(t time.Time) error {
	err1 := c.SetReadDeadline(t)
	err2 := c.SetWriteDeadline(t)

	if err1 == nil {
		return err1
	}
	return err2
}

func (connection) LocalAddr() net.Addr                { return address{} }
func (connection) RemoteAddr() net.Addr               { return address{} }
func (connection) Read(b []byte) (n int, err error)   { return os.Stdin.Read(b) }
func (connection) Write(b []byte) (n int, err error)  { return os.Stdout.Write(b) }
func (connection) SetReadDeadline(t time.Time) error  { return os.Stdin.SetReadDeadline(t) }
func (connection) SetWriteDeadline(t time.Time) error { return os.Stdout.SetWriteDeadline(t) }

type address struct{}

func (address) Network() string { return "stdio" }
func (address) String() string  { return "stdio" }
