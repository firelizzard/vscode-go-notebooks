package main

import (
	"fmt"
	"io/ioutil"
	"runtime"

	"github.com/traefik/yaegi/interp"
	"github.com/traefik/yaegi/stdlib"
)

func main() {
	i := interp.New(interp.Options{
		Stdout: ioutil.Discard,
		Stderr: ioutil.Discard,
	})

	i.Use(stdlib.Symbols)

	_, err := i.Eval(`panic("foobar")`)

	perr := err.(interp.Panic)
	frames := runtime.CallersFrames(perr.Callers)
	for {
		frame, more := frames.Next()

		fmt.Printf("%v:%v\n", frame.File, frame.Line)

		if !more {
			break
		}
	}
}
