import * as CSV from 'papaparse'
import $ from 'cash-dom'

const notebookApi = acquireNotebookRendererApi('go-notebooks-renderer')

notebookApi.onDidCreateOutput(e => {
    switch (e.mime) {
    case 'text/csv':
        parseCSV(e.outputId, e.value, e.element)
    }
})


function parseCSV(outputId: string, value: string, element: HTMLElement) {
    const { data } = CSV.parse<string[]>(value)
    if (!data.length) return

    let style = $(`style[output-id="${outputId}"]`)
    if (!style.length)
        style = $(`<style output-id="${outputId}">`).appendTo('head')
    style.text(`
        [id="${outputId}"].output main {
            overflow: auto;
            white-space: nowrap;
            max-height: 40em;
        }

        [id="${outputId}"].output th {
            position: sticky;
            top: 0;
            background: var(--vscode-notebook-outputContainerBackgroundColor);
        }
    `)

    const wrapper = $('<main>').appendTo(element)
    const table = $('<table>').appendTo(wrapper)

    const header = data[0]
    if (header.every(x => x.match(/^[\w ]+$/)) && new Set(header).size == header.length) {
        data.splice(0, 1)

        const tr = $('<tr>').appendTo($('<thead>').appendTo(table))
        header.forEach(v => $('<th>').appendTo(tr).append($('<div>').text(v)))
    }

    const body = $('<tbody>').appendTo(table)

    for (const row of data) {
        if (!row.some(x => x.length > 0))
            continue

        const tr = $('<tr>').appendTo(body)
        row.forEach(v => $('<td>').appendTo(tr).text(v))
    }

    requestAnimationFrame(() => {
        const { backgroundColor } = getComputedStyle(table.find('thead')[0]!)
        const { padding } = getComputedStyle(table.find('th')[0]!)

        style[0]!.innerHTML += `
            [id="${outputId}"].output th {
                padding: 0;
            }
            [id="${outputId}"].output th > div {
                padding: ${padding};
                background-color: ${backgroundColor};
            }
        `
    })
}