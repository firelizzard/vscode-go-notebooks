import * as vscode from 'vscode';

import { ContentProvider } from './content';
import { KernelProvider } from './kernel';


export function activate(context: vscode.ExtensionContext) {
	const content = new ContentProvider()
	const kernel = new KernelProvider()

	context.subscriptions.push(vscode.notebook.registerNotebookContentProvider('golang-notebook', content, content.options))
	context.subscriptions.push(vscode.notebook.registerNotebookKernelProvider({ viewType: ['go', 'golang-notebook']}, kernel))
	context.subscriptions.push(vscode.notebook.registerNotebookKernelProvider({ filenamePattern: '*.{gonb,gonotebook}' }, kernel))

	context.subscriptions.push(vscode.commands.registerCommand('goNotebooks.kernel.restart', () => {
		kernel.kernel.kill('SIGTERM')
	}))

	context.subscriptions.push(vscode.commands.registerCommand('goNotebooks.kernel.rebuild', async () => {
		await kernel.kernel.install(false)
	}))

	context.subscriptions.push(vscode.commands.registerCommand('goNotebooks.kernel.stopSession', async () => {
		const uri = vscode.window.activeNotebookEditor?.document?.uri
		if (uri) kernel.kernel.stopSession(uri)
	}))
}

// this method is called when your extension is deactivated
export function deactivate() {}