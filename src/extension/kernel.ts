import path = require('path')
import { promises as fsp } from 'fs'
import vscode = require('vscode')
import grpc = require('@grpc/grpc-js')
import { ChildProcess, spawn } from 'child_process'
import { KernelClient } from './proto/kernel_grpc_pb'
import { EvaluateRequest, PromptResponse, ToClient, ToServer, CacheResponse, CancelEvaluate } from './proto/kernel_pb'

const KernelModule = 'gitlab.com/firelizzard/vscode-go-notebooks/kernel/cmd/notebook-kernel'
const KernelVersion = 'latest'
const KernelExecutable = 'notebook-kernel'

const { F_OK, X_OK } = require('fs').constants

async function canAccess(path: string, mode: number) {
    try {
        await fsp.access(path, mode)
        return true
    } catch (error) {
        return false
    }
}

function assertCanAccess(path: string, mode: number, err: any) {
    canAccess(path, mode).then(x => x || Promise.reject(err))
}

function waitForProc(label: string, proc: ChildProcess): Promise<undefined>
function waitForProc(label: string, proc: ChildProcess, getOutput: () => string): Promise<string>
function waitForProc(label: string, proc: ChildProcess, getOutput?: () => string): Promise<string | undefined> {
    return new Promise((resolve, reject) => {
        proc.on('error', err => reject({ message: `An error occured while executing ${label}: ${err.message}`}))

        proc.on('exit', (code, signal) => {
            const out = getOutput && getOutput()

            if (signal)
                reject({ message: `${label} was terminated with signal ${signal}` })
            else if (code === 0)
                resolve(out)
            else if (out && out.length)
                reject({ message: `${label} exited with code ${code}:\n${out}` })
            else
                reject({ message: `${label} exited with code ${code}` })
        })
    })
}

function readCmd(label: string, path: string, args?: string[]): Promise<string> {
    const proc = spawn(path, args)

    let out = ''
    proc.stdout.on('data', b => out += b.toString())
    proc.stderr.on('data', b => out += b.toString())

    return waitForProc(label, proc, () => out)
}

type Session = grpc.ClientDuplexStream<ToServer, ToClient>
type Cache = { [key: string]: string }

class Kernel implements vscode.NotebookKernel {
    static get module() {
        return vscode.workspace.getConfiguration('goNotebooks').get('kernel.module') || KernelModule
    }

    static get version() {
        return vscode.workspace.getConfiguration('goNotebooks').get('kernel.version') || KernelVersion
    }

    static get output() {
        const value = vscode.window.createOutputChannel('Go Notebook Kernel')
        Object.defineProperty(this, 'output', { value })
        return value
    }

    private connectTo: number | undefined

    constructor() {
        // this.connectTo = 12345
    }

    label = 'Go Kernel'
    id = 'go-kernel'
    supportedLanguages = ['go']

    private proc: ChildProcess | undefined
    private kernel: KernelClient | undefined
    private sessions = new Map<vscode.Uri, Session>()
    private cache = new Map<vscode.Uri, Cache>()
    private diagnostics = vscode.languages.createDiagnosticCollection()

    kill(signal?: NodeJS.Signals | number) {
        const sessions = Array.from(this.sessions.values())
        this.sessions.clear()

        for (const session of sessions)
            session.cancel()

        return this.proc?.kill(signal) || false
    }

    interrupt?(document: vscode.NotebookDocument): void {
        const session = this.sessions.get(document.uri)
        if (session) session.write(new ToServer().setCancel(new CancelEvaluate()))
    }

    async executeCellsRequest(document: vscode.NotebookDocument, ranges: vscode.NotebookCellRange[]): Promise<void> {
        if (!this.kernel) {
            try {
                await this.launch()
            } catch (error) {
                Kernel.output.append(error.message)
                vscode.window.showErrorMessage(error.message)
                return
            }
        }

        let session = this.sessions.get(document.uri)
        if (!session) {
            session = this.kernel!.session()
            this.sessions.set(document.uri, session)
        }

        for (const range of ranges) {
            for (let i = range.start; i < range.end; i++) {
                const task = vscode.notebook.createNotebookCellExecutionTask(document.uri, i, this.id)
                try {
                    await this.execute(session, task!)
                } catch (error) {
                    console.log(error)
                }
            }
        }
    }

    stopSession(uri: vscode.Uri) {
        const session = this.sessions.get(uri)
        if (!session) return

        session.cancel()
        this.sessions.delete(uri)
    }

    private async goCmdPath() {
        const config = vscode.workspace.getConfiguration('goNotebooks')
        let goCmd: string | undefined = config.get('go.path')
        if (goCmd) {
            await assertCanAccess(goCmd, F_OK, { message: `'${goCmd}' does not exist or cannot be accessed` })
            await assertCanAccess(goCmd, X_OK, { message: `'${goCmd}' cannot be executed` })
            return goCmd
        }

        await assertCanAccess('go', F_OK | X_OK, { message: `Cannot find 'go'. Please add 'go' to the PATH or configure 'goNotebook.go.path'.` })
        return 'go'
    }

    private async path() {
        const config = vscode.workspace.getConfiguration('goNotebooks')
        const kernelPath: string | undefined = config.get('kernel.path')
        if (kernelPath) {
            await assertCanAccess(kernelPath, F_OK, { message: `'${kernelPath}' does not exist or cannot be accessed` })
            await assertCanAccess(kernelPath, X_OK, { message: `'${kernelPath}' cannot be executed` })
            return kernelPath
        }

        const goCmd = await this.goCmdPath()

        const goBin = (await readCmd("`go env GOBIN`", goCmd, ['env', 'GOBIN'])).trim()
        const goBinPath = path.join(goBin, KernelExecutable)
        if (goBin.length && await canAccess(goBinPath, F_OK | X_OK))
            return goBinPath

        const goPath = (await readCmd("`go env GOPATH`", goCmd, ['env', 'GOPATH'])).trim()
        const goPathPath = path.join(goPath.split(';')[0], 'bin', KernelExecutable)
        if (goPath.length && await canAccess(goPathPath, F_OK | X_OK))
            return goPathPath
    }

    async install(ask = true) {
        const config = vscode.workspace.getConfiguration('goNotebooks')
        const tags: string | undefined = config.get('kernel.tags')
        const modVer = `${Kernel.module}@${Kernel.version}`
        const label = `"go get${tags ? '-tags ' + tags : ''} ${modVer}"`

        if (ask) {
            const sel = await vscode.window.showErrorMessage(`The Go notebook kernel is not available. Run ${label} to install.`, 'Install')
            if (sel != 'Install') return
        }

        const args = ['get', '-u', modVer]
        if (tags) args.splice(1, 0, '-tags', tags)

        Kernel.output.clear()
        Kernel.output.append(`Installing kernel: go ${args.join(' ')}\n\n`)
        Kernel.output.show()

        const env = Object.assign({}, process.env, { GO111MODULE: 'on' })
        const proc = spawn(await this.goCmdPath(), args, { env })
        proc.stdout.on('data', b => Kernel.output.append(b.toString()))
        proc.stderr.on('data', b => Kernel.output.append(b.toString()))

        await waitForProc(label, proc)

        const path = await this.path()
        if (path) return path
        
        vscode.window.showErrorMessage('Failed to install kernel')
    }

    private async launch() {
        if (this.connectTo) {
            this.kernel = new KernelClient(`localhost:${this.connectTo}`, grpc.credentials.createInsecure())
            return
        }

        const kernelPath = await this.path() || await this.install()
        if (!kernelPath) return

        this.proc = spawn(kernelPath, ['tcp', 'localhost:'])

        this.proc.stdout!.on('data', b => Kernel.output.appendLine(`[Kernel] ${b.toString()}`))
        this.proc.stderr!.on('data', b => Kernel.output.appendLine(`[Kernel] ${b.toString()}`))

        this.proc.on('exit', () => {
            this.proc = void 0
            this.kernel = void 0

            // cancel and remove all sessions
            for (const session of this.sessions.values())
                session.cancel()
            this.sessions.clear()
        })

        const listening = await Promise.race([
            waitForProc('Kernel', this.proc).then(() => Promise.reject('Kernel exited with code 0')),
            new Promise<string>(resolve => {
                let s = ''
                this.proc!.stdout!.on('data', (b: Buffer) => {
                    s += b.toString('utf-8')
                    if (s.indexOf('\n') >= 0)
                        resolve(s)
                })
            })
        ])

        const [, port] = /^Listening on .*:(\d+)\n/.exec(listening) as string[]

        this.kernel = new KernelClient(`localhost:${port}`, grpc.credentials.createInsecure())
    }

    private async execute(session: Session, task: vscode.NotebookCellExecutionTask) {
        task.start()
        task.clearOutput()
        this.diagnostics.set(task.cell.document.uri, [])

        let resolve: (_: vscode.NotebookCellExecuteEndContext) => void
        const done = new Promise<vscode.NotebookCellExecuteEndContext>((r, j) => (resolve = r))

        const handle = async (msg: ToClient) => {
            switch (msg.getKindCase()) {
            case ToClient.KindCase.OUTPUT:
                {
                    const items = msg.getOutput()!.getContentList().map(c => {
                        let mime = c.getMime()
                        let value = c.getValue()
                        
                        value = Buffer.from(value).toString('utf-8')
        
                        switch (mime) {
                        case 'stdout':
                            mime = 'application/x.notebook.stdout'
                            break
        
                        case 'stderr':
                            mime = 'application/x.notebook.stderr'
                            break
        
                        case 'application/json':
                            try {
                                value = JSON.parse(value)
                            } catch (e) {}
                            break
                        }

                        return new vscode.NotebookCellOutputItem(mime, value)
                    })
                    
                    task.appendOutput([ new vscode.NotebookCellOutput(items) ])
                }
                break

            case ToClient.KindCase.EVALUATE:
                {
                    const resp = msg.getEvaluate()!.toObject()
                    const result: vscode.NotebookCellExecuteEndContext = {
                        success: true,
                    }

                    if (resp.duration) {
                        const { seconds, nanos } = resp.duration
                        result.duration = seconds * 1000 + nanos / 1000000
                    }

                    for (const err of resp.errorsList) {
                        result.success = false

                        if (err.position) {
                            const lineNo = err.position.line - 1
                            const column = err.position.column - 1
                            const line = task.cell.document.lineAt(lineNo)                   

                            this.diagnostics.set(task.cell.document.uri, [
                                new vscode.Diagnostic(new vscode.Range(lineNo, column, lineNo, line.text.length - column), err.message, vscode.DiagnosticSeverity.Error)
                            ])
                        }

                        task.appendOutput([ 
                            new vscode.NotebookCellOutput([
                                new vscode.NotebookCellOutputItem('application/x.notebook.stderr', err.message)
                            ])
                        ])
                    }

                    session.off('data', handle)
                    resolve(result)
                }
                break

            case ToClient.KindCase.PROMPT:
                {
                    const { placeholder, ...opts } = msg.getPrompt()!.toObject()
                    const answer = await vscode.window.showInputBox({ ...opts, placeHolder: placeholder })

                    const resp = new PromptResponse()
                    if (answer) resp.setValue(answer)

                    session.write(new ToServer().setPrompt(resp))
                }
                break

            case ToClient.KindCase.CACHE:
                {
                    let cache = this.cache.get(task.cell.notebook.uri)
                    if (!cache)
                        this.cache.set(task.cell.notebook.uri, (cache = {}))

                    const req = msg.getCache()!.toObject()
                    const resp = new CacheResponse()
                    if (req.key in cache)
                        resp.setValue(cache[req.key])
                    
                    if (req.write)
                        cache[req.key] = req.write

                    session.write(new ToServer().setCache(resp))
                }
                break
            }
        }

        session.on('end', () => {
            // if the session ends early
            session.off('data', handle)
            resolve({})
        })

        session.on('data', handle)

        const req = new ToServer()
        const evalReq = new EvaluateRequest()
        evalReq.setCode(task.cell.document.getText())
        req.setEvaluate(evalReq)
        session.write(req)

        task.end(await done)
    }
}

export class KernelProvider implements vscode.NotebookKernelProvider<Kernel> {
    readonly kernel = new Kernel

    provideKernels(document: vscode.NotebookDocument, token: vscode.CancellationToken): vscode.ProviderResult<Kernel[]> {
        return [this.kernel]
    }
}