import * as vscode from 'vscode';
import { loadMarkdown, storeMarkdown } from './markdown';

export class ContentProvider implements vscode.NotebookContentProvider {
    options: vscode.NotebookDocumentContentOptions = {
        transientMetadata: {
            editable: true,
            custom: true,
        },
        transientOutputs: true
    };

    onDidChangeNotebookContentOptions?: vscode.Event<vscode.NotebookDocumentContentOptions>;

    async resolveNotebook(document: vscode.NotebookDocument, webview: vscode.NotebookCommunication): Promise<void> {
        
    }
    
    async openNotebook(uri: vscode.Uri, openContext: vscode.NotebookDocumentOpenContext, token: vscode.CancellationToken): Promise<vscode.NotebookData> {
        if (openContext.backupId) {
            uri = vscode.Uri.parse(openContext.backupId)
        }

		const content = Buffer.from(await vscode.workspace.fs.readFile(uri)).toString('utf8')

        return {
            metadata: new vscode.NotebookDocumentMetadata().with({
                editable: true,
                cellEditable: true,
                cellHasExecutionOrder: false,
            }),
            cells: Array.from(loadMarkdown(content))
        }
    }

    async saveNotebookAs(targetResource: vscode.Uri, document: vscode.NotebookDocument, token: vscode.CancellationToken): Promise<void> {
        const md = storeMarkdown(document.cells);
		await vscode.workspace.fs.writeFile(targetResource, Buffer.from(md));
    }

    saveNotebook(document: vscode.NotebookDocument, token: vscode.CancellationToken): Promise<void> {
        return this.saveNotebookAs(document.uri, document, token)
    }
    
    async backupNotebook(document: vscode.NotebookDocument, context: vscode.NotebookDocumentBackupContext, token: vscode.CancellationToken): Promise<vscode.NotebookDocumentBackup> {
        await this.saveNotebookAs(context.destination, document, token)

        return {
            id: context.destination.toString(),
			delete: () => vscode.workspace.fs.delete(context.destination)
        }
    }
}