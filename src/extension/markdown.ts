import * as vscode from 'vscode';

type Plain<T> = Pick<T, keyof T>

interface StoredData {
    outputs: StoredOutput[]
}

interface StoredOutput extends Omit<vscode.NotebookCellOutput, 'outputs'> {
    outputs: Plain<vscode.NotebookCellOutputItem>[]
}

export function storeMarkdown(cells: ReadonlyArray<vscode.NotebookCell>): string {
    return cells.map(cell => {
        if (cell.kind !== vscode.NotebookCellKind.Code)
            return cell.document.getText()

        let s = `${'```'}${cell.document.languageId}\n${cell.document.getText()}\n${'```'}`
        if (cell.outputs.length)
            s += JSON.stringify({ outputs: cell.outputs })
        return s
    }).join('\n\n')
}

export function* loadMarkdown(content: string): Generator<vscode.NotebookCellData> {
    let language: string | null = null
    const lines = []

    for (const line of content.split(/\r?\n/g)) {
        if (!line.startsWith('```')) {
            lines.push(line)
            continue
        }

        if (lines.length > 0) {
            let source = lines.splice(0).join('\n')
            let outputs: vscode.NotebookCellOutput[] | undefined

            if (line.length > 3)
                try {
                    const data: StoredData = JSON.parse(line.substring(3))

                    outputs = data.outputs.map(({ id, metadata, outputs }) => {
                        const items = outputs.map(({ mime, value, metadata }) =>
                            new vscode.NotebookCellOutputItem(mime, value, metadata)
                        )
                        return new vscode.NotebookCellOutput(items, id, metadata)
                    })
                } catch (e) {}

            if (!language)
                source = source.trim()

            if (language || source)
                yield {
                    kind: language === null ? vscode.NotebookCellKind.Markdown : vscode.NotebookCellKind.Code,
                    language: language || '',
                    outputs,
                    source,
                    metadata: new vscode.NotebookDocumentMetadata().with({
                        editable: true,
                    })
                }
        }

        if (language)
            language = null;
        else
            language = line.substring(3)
    }

    let source = lines.splice(0).join('\n')

    if (!language)
        source = source.trim()

    if (language || source)
        yield {
            kind: language === null ? vscode.NotebookCellKind.Markdown : vscode.NotebookCellKind.Code,
            language: language || '',
            outputs: [],
            source
        }
}